#!/bin/sh
sudo pacman -Syyu --noconfirm \
    git \
    zsh \
    zsh-completions \
    zsh-autosuggestions \
    zsh-syntax-highlighting
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | zsh
sudo usermod --shell /bin/zsh $USER
nvm install --lts
nvm use --lts
npm install -g yarn
sudo pacman -S --noconfirm \
    xf86-video-intel \ # intel graphics card
    xorg \             # display server
    xorg-xinit \       # used to start WM
    xorg-xrdb \        # user configuration
    nitrogen \         # desktop background
    picom \            # compositor
    atom \
    docker \
    dolphin \
    gcc \
    gimp \
    github-cli \
    go \
    htop \
    keepassxc \
    libreoffice-still \
    monero-gui \
    mpv \
    neofetch \
    obs-studio \
    pyenv \
    qbittorrent \
    qtile \
    terminator \
    tor \
    torbrowser-launcher \
    vim \
    virtualbox \
    virtualbox-host-modules-arch \
    wget \
    youtube-dl
sudo cp /etc/X11/xinit/xinitrc $HOME/.xinitrc
mkdir -p $HOME/.config/qtile/ && sudo cp /usr/share/doc/qtile/default_config.py $HOME/.config/qtile/config.py
sudo chown $USER:wheel .config/qtile/config.py
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si
cd
yay -Syyu --noconfirm brave \
    librewolf \
    protonmail-bridge \
    session-desktop \
    slack-desktop \
    ttf-meslo-nerd-font-powerlevel10k \
    zoom \
    zsh-theme-powerlevel10k-git
echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >> $HOME/.zshrc
apm install 90s-hack-syntax \
    90s-hack-ui \
    atom-autocomplete-python \
    atom-handlebars \
    atom-jinja2
    block-cursor \
    busy-signal \
    color-picker \
    docker \
    editor-background \
    emmet \
    expressjs \
    flask-snippets \
    hightlight-selected \
    ide-docker \
    intentions \
    language-docker \
    language-gemini \
    linter \
    linter-docker \
    linter-eslint \
    linter-flake8 \
    linter-pyflakes \
    linter-pylint \
    linter-ui-default \
    markdown writter \
    pigments \
    platformio-ide-terminal \
    prettier-atom \
    python-autopep8 \
    python-black \
    react \
    smart-gitignore \
    tidy-markdown \
    tool-bar \
    tool-bar-markdown-writer \
    zentabs
